
let firstname = document.getElementById('1name')
   let Lastname =  document.getElementById('lname')
    let Age =  document.getElementById('age')
    let searchInput =  document.getElementById('ser')
    let isAdmin = false;

document.addEventListener('DOMContentLoaded', ()=>{
    let searchFilter;
    searchInput.addEventListener('change',(e)=>{
        searchFilter = e.target.value;
        usersr(searchFilter);
        console.log(e.target.value)
    })

    usersr(searchFilter)
    document.getElementById("save").onclick = async function(){
        const users = await fetch("/user/all").then(resp => resp.json());

        await fetch(
            "/user",
            {
                method: "POST",
                headers: {
                    "Content-type": "application/json"
                },
                body: JSON.stringify({
                    firstname: firstname.value,
                    Lastname: Lastname.value,
                    Age: Age.value,
                    isAdmin: isAdmin //Boolean
                })
            }
        ).then(resp => resp.json())

        usersr(searchFilter)
    }



    async function usersr(filter){
        let showtable;
        if(!filter){
            showtable = await fetch("/user/all")
                .then(response => response.json())
                .then(json => json)
        } else{
            showtable = await fetch(`/user/all?filter=${filter}`)
                .then(response => response.json())
                .then(json => json)
        }


        document.getElementById('f').innerHTML = '';
        document.getElementById('i').innerHTML = '';
        document.getElementById('l').innerHTML = '';
        document.getElementById('a').innerHTML = '';
        document.getElementById('is').innerHTML = '';
        document.getElementById('d').innerHTML = '';
        showtable.map((el)=>{
            document.getElementById('f').insertAdjacentHTML('beforeEnd', `<p>${el.firstname}</p>`);
            document.getElementById('i').insertAdjacentHTML('beforeEnd', `<p>${el._id}</p>`);
            document.getElementById('l').insertAdjacentHTML('beforeEnd', `<p>${el.Lastname}</p>`);
            document.getElementById('a').insertAdjacentHTML('beforeEnd', `<p>${el.Age}</p>`);
            document.getElementById('is').insertAdjacentHTML('beforeEnd', `<p>${el.isAdmin}</p>`);

            let newButton = document.createElement("button");
            newButton.innerHTML = 'delet'
            newButton.addEventListener('click', async ()=>{
                await fetch(
                    "/user",
                    {
                        method: "DELETE",
                        headers: {
                            "Content-type": "application/json"
                        },
                        body: JSON.stringify({
                            name: el.firstname,
                        })
                    }
                ).then(resp => {
                    usersr();
                    return resp.json();

                })

            })
            document.getElementById('d').insertAdjacentElement('beforeEnd', newButton);
        })

    }


});
